package org.slingerxv.limitart.game.mission;

import org.slingerxv.limitart.base.EnumInterface;

/**
 * 任务类型
 *
 * @author hank
 * @version 2018/4/13 0013 19:48
 */
@EnumInterface
public interface MissionType {
}
