package org.slingerxv.limitart.game.org;

import org.slingerxv.limitart.base.EnumInterface;

/**
 * 权限(建议使用枚举构造一组Auth)
 *
 * @author hank
 */
@EnumInterface
public interface Auth {
    int authID();
}
