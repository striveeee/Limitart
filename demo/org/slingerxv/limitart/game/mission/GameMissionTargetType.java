package org.slingerxv.limitart.game.mission;

/**
 * @author hank
 * @version 2018/4/13 0013 20:20
 */
public enum GameMissionTargetType implements MissionTargetType {
    /**
     * 杀怪目标类型
     */
    KILL_MONSTER,
    /**
     * 采集
     */
    COLLECT,
}
