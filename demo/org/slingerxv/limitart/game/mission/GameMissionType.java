package org.slingerxv.limitart.game.mission;

/**
 * @author hank
 * @version 2018/4/13 0013 20:20
 */
public enum GameMissionType implements MissionType {
    /**
     * 日常任务
     */
    DAILY,
}
